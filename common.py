#-*- coding: UTF-8 -*-
enterDebug = False

def debug(module, msg):
    '''
    use it by
    common.debug(__name__, 'string')
    '''
    if enterDebug:
        print module, ':', msg

if __name__ == '__main__':
    debug('common')