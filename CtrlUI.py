#-*- coding: UTF-8 -*-
import sys
#from PyQt4 import QtCore, QtGui, uic
from PyQt4 import QtGui
import common
from ui import Ui_MainWindow 
from DriverSerial import SerialSettings

class CtrlWindowUI(QtGui.QMainWindow, Ui_MainWindow):
    def __init__(self, parent = None):
        super(CtrlWindowUI, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)       #pyuic4 -o ui.py ui.ui
        #self.ui = uic.loadUi('ui.ui', self)        #QtDesigner ui.ui
        self.parityType = {'None'   : 'N',
                           'Odd'    : 'O',
                           'Even'   : 'E',
                           'Mark'   : 'M',
                           'Space'  : 'S'
                           }

    def setStatusLBText(self, msg):             
        self.ui.statusLB.setText(msg)

    def setDispTEText(self, msg):             
        self.ui.dispTE.setText(msg)
        
    def setDispTERecv(self, msg):
        self.tc = self.ui.dispTE.textCursor()
        self.tc.movePosition(self.tc.End)   
        self.ui.dispTE.setTextCursor(self.tc)              
        self.ui.dispTE.insertPlainText(msg)
        for c in msg:
            if c == '\n':
                self.vsb = self.ui.dispTE.verticalScrollBar()
                self.vsb.setSliderPosition(self.vsb.maximum())   
                        
    def setDispTEClear(self):             
        self.ui.dispTE.clear()
        
    def dispMsgBox(self, title, content):
        QtGui.QMessageBox.about(self, title, content)       
        
    def setCursor(self):
        self.tc = self.ui.dispTE.textCursor()
        self.tc.movePosition(QtGui.QTextCursor.End)  
          
    def closeEvent(self, e):
        common.debug(__name__, 'closeEvent')
        
    def changeOpenButton(self, opend):
        if opend:
            self.ui.openPB.setText(QtGui.QApplication.translate("MainWindow", "关闭", None, QtGui.QApplication.UnicodeUTF8))
        else:
            self.ui.openPB.setText(QtGui.QApplication.translate("MainWindow", "打开", None, QtGui.QApplication.UnicodeUTF8))
    
    def getSerialSetting(self):
        SerialSettings['port'] = self.ui.portCB.currentText().toUtf8().data()
        SerialSettings['baund'] = int(self.ui.baundCB.currentText().toUtf8().data())
        SerialSettings['bytesize'] = int(self.ui.bytesizeCB.currentText().toUtf8().data())
        SerialSettings['stopbits'] = float(self.ui.stopbitsCB.currentText().toUtf8().data())
        self.parity = self.ui.parityCB.currentText().toUtf8().data()
        for self.name, self.shortName in self.parityType.items(): 
            if self.parity == self.name:
                SerialSettings['parity'] = self.shortName
                
    def resizeEvent(self, e):
        self.msize = self.size()       
        self.msize.setHeight(self.msize.height() - 100)
        self.msize.setWidth(self.msize.width() - 10)
        self.ui.dispTE.resize(self.msize)
                
    def setDispTEFocus(self):
        self.ui.dispTE.setFocus()
                  
if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    win = CtrlWindowUI()
    win.show()
    sys.exit(app.exec_())